import * as type from '../types';
export const loginAction = (payload:any) => {
    
    return {
        type : type.LOGIN_REQUEST,
        payload,
    }
}
export const getProfileAction = (token:any) => {
    
    return {
        type : type.GET_PROFILE_REQUEST,
        payload:token,
    }
}