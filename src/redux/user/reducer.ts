import { createSlice } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit'
import { getPaging } from '@/utils/helper';
import { removeAxiosToken, setAxiosToken } from '@/api/requester';

const initialState: any = {
    isLoading: false,

    token: null,
    loginFailed: null,

    profile: null,
    isGetProfileLoading: false,
    getProfileFailed: null,

}

export const user = createSlice({
    name: 'user',
    initialState,
    reducers: {
        // LOGIN
        loginRequest: (state, { payload }) => {
            state.isLoading = true;
            state.token = null;
            state.loginFailed = null;
        },
        loginSuccess: (state, { payload }) => {
            state.token = payload.token;
            state.profile = payload.profile;
            setAxiosToken(payload.token);
        },
        loginFailed: (state, { payload }) => {
            state.loginFailed = payload;
            state.isLoading = false;
        },
        logoutRequest: (state, { payload }) => {
            removeAxiosToken();
            localStorage.clear();
        },

        // GET PROFILE

        getProfileRequest: (state, { payload }) => {
            state.isGetProfileLoading = true;
            state.getProfileFailed = null;
        },
        getProfileSuccess: (state, { payload }) => {
            state.isGetProfileLoading = false;
            state.profile = payload;
        },
        getProfileFailed: (state, { payload }) => {
            state.isGetProfileLoading = false;
            state.getProfileFailed = payload;
        },
    },
})

// Action creators are generated for each case reducer function
export const actions = user.actions

export default user.reducer