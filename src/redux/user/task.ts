import * as type from '../types'
import { call, put, takeLatest } from "redux-saga/effects";
import api from '@/api';
import { actions } from './reducer';
function * login({payload}:any): Generator<any, void, any>{
    try {
        const user = yield call(api.user.login,payload);
        yield put(actions.loginSuccess(user));
    } catch (error) {
        yield put(actions.loginFailed(error));
    }
}
function * getProfile({payload}:any): Generator<any, void, any>{
    try {
        const profile = yield call(api.user.getProfile,payload);
        yield put(actions.getProfileSuccess(profile));
    } catch (error) {
        yield put(actions.getProfileFailed(error));
    }
}
export default function* saga(){
    yield takeLatest(type.LOGIN_REQUEST,login)
    yield takeLatest(type.GET_PROFILE_REQUEST,getProfile)
}