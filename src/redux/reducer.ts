import { combineReducers } from "redux";
import { persistReducer } from 'redux-persist';

import storage from "redux-persist/lib/storage";
import todoList from './todoList/reducer';
import user from './user/reducer';
const userPersistConfig = {
    key: 'user',
    storage: storage,
    blacklist: [
        'loginFailed',
        'isLoading',
        'isGetProfileLoading',
        'getProfileFailed'
      ]
  };
const rootReducer = combineReducers({
    user: persistReducer(userPersistConfig, user),
    todoList
});
export default rootReducer