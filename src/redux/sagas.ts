import { all } from 'redux-saga/effects';
import todoList from './todoList/task'
import user from './user/task'
export default function* rootSaga() {
    yield all([
        todoList(),
        user(),
    ]);
  }