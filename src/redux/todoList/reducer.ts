import { createSlice } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit'
import { getPaging } from '@/utils/helper';

const initialState: any = {
    isLoading: false,
    getListFailed: undefined,
    list: [],

    isGetByIdLoading: false,
    byId: null,
    getByIdFailed: null,

    deleteSuccess: null,
    deleteFailed: null,

    isSubmitLoading: false,
    createSuccess: null,
    createFailed: null,

    updateSuccess: null,
    updateFailed: null,

    paging: null
}

export const todoList = createSlice({
    name: 'todoList',
    initialState,
    reducers: {
        // Get All
        getListRequest: (state, { payload }) => {
            state.isLoading = true;
            state.list = [];
            state.getListFailed = null;
        },
        getListSuccess: (state, { payload }) => {
            state.isLoading = false;
            state.list = payload;
            state.paging = getPaging(payload);
        },
        getListFailed: (state, { payload }) => {
            state.isLoading = false;
            state.getListFailed = null;
        },

        // Get By Id
        getByIdRequest: (state, { payload }) => {
            state.isGetByIdLoading = true;
            state.byId = null;
            state.getByIdFailed = null;
        },
        getByIdSuccess: (state, { payload }) => {
            state.isGetByIdLoading = false;
            state.byId = payload;
        },
        getByIdFailed: (state, { payload }) => {
            state.isGetByIdLoading = false;
            state.getByIdFailed = payload;
        },
        //Create
        createRequest: (state, { payload }) => {
            state.isSubmitLoading = true;
            state.createSuccess = null;
            state.createFailed = null;
        },
        createSuccess: (state, { payload }) => {
            state.isSubmitLoading = false;
            state.createSuccess = payload;
        },
        createFailed: (state, { payload }) => {
            state.isSubmitLoading = false;
            state.createFailed = payload;
        },
        //Update
        updateRequest: (state, { payload }) => {
            state.isSubmitLoading = true;
            state.updateSuccess = null;
            state.updateFailed = null;
        },
        updateSuccess: (state, { payload }) => {
            state.isSubmitLoading = false;
            state.updateSuccess = payload;
            state.byId = payload;
            state.list = state.list.map((item: any) => {
                if (item.id === payload.id) return { ...item, ...payload }
                return item
            })
        },
        updateFailed: (state, { payload }) => {
            state.isSubmitLoading = false;
            state.updateFailed = payload;
        },
        //Delete
        deleteRequest: (state, { payload }) => {
            state.isLoading = true;
            state.deleteSuccess = null;
            state.deleteFailed = null;
        },
        deleteSuccess: (state, { payload }) => {
            state.deleteSuccess = payload;
            state.isLoading = false;
        },
        deleteFailed: (state, { payload }) => {
            state.isLoading = false;
            state.deleteFailed = payload;
        },

        // Reset state
        resetStore: (state) => {
            state = initialState;
        },
    },
})

// Action creators are generated for each case reducer function
export const actions = todoList.actions

export default todoList.reducer