export const GET_TODO_LIST_REQUEST = 'GET_TODO_LIST_REQUEST';
export const GET_TODO_LIST_SUCCESS = 'GET_TODO_LIST_SUCCESS';
export const GET_TODO_LIST_FAILED = 'GET_TODO_LIST_FAILED';