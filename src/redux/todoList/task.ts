import * as type from '../types'
import { call, put, takeLatest } from "redux-saga/effects";
import api from '@/api';
import { actions } from './reducer';
function * getListRequest({payload}:any): Generator<any, void, any>{
    try {
        const list = yield call(api.todoList.getAll,payload);
        yield put(actions.getListSuccess(list));
    } catch (error) {
        yield put(actions.getListFailed(error));
    }
}
export default function* saga(){
    yield takeLatest(type.GET_TODO_LIST_REQUEST,getListRequest)
}