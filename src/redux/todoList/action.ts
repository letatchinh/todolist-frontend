import * as type from '../types';
export const getListAction = (payload:any) => {
    return {
        type : type.GET_TODO_LIST_REQUEST,
        payload,
    }
}