"use client";
import "./globals.css";
import type { Metadata } from "next";
import { Inter } from "next/font/google";
import Header from "@/components/layout/Header";
import Footer from "@/components/layout/Footer";
import "@/styles/index.scss";
import SideBar from "@/components/layout/SideBar";
import { Head } from "next/document";
import { Toaster } from "react-hot-toast";
import { Provider } from "react-redux";
import { persistor, store } from "@/redux/stores";
import MainContent from "@/components/layout/MainContent";
import { PersistGate } from "redux-persist/lib/integration/react";
const inter = Inter({ subsets: ["latin"] });

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <link
        precedence="default"
        rel="stylesheet"
        href="https://unicons.iconscout.com/release/v4.0.8/css/line.css"
      />

      <body className={inter.className}>
        <Provider store={store}>
          <PersistGate loading={<div>...</div>} persistor={persistor}>
          <Toaster />
          <Header />
          <MainContent>
            {children}
          </MainContent>
          </PersistGate>
        </Provider>
        {/* <Footer/> */}
      </body>
    </html>
  );
}
