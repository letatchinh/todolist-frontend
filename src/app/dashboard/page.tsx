
'use client'
import TodoItem from "./TodoItem";
import useSWR from 'swr';
import api from "@/api";
import { BASE_URL } from "@/constants/defaultValue";
import { fetcher } from "@/utils/helper";
import { setupAxios } from "@/api/requester";
export default function page() {
  const {data} = useSWR({page : 1,limit : 10},api.todoList.getAll);
  console.log(data,'data');
  
  return (
    <div className="page-content">
      <div className="todoList">
        <div className="todoList-new">
          <ul className="todoList-new--list">
            {data?.docs?.map((todo:any) => (
              <TodoItem todo={todo}/>
            ))}
          </ul>
        </div>
      </div>
    </div>
  );
}
