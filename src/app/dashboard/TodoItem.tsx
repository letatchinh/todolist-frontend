"use client";
import { AntDesignOutlined, CloseCircleOutlined, CloseCircleTwoTone, CommentOutlined, InfoCircleTwoTone, PaperClipOutlined, UserOutlined } from "@ant-design/icons";
import { Avatar, Col, Progress, Row, Tag, Tooltip, Typography } from "antd";
import moment from "moment";
import React from "react";

export default function TodoItem({todo}:any) {
  const {title,desc,todoSeq,tags,status,isImportant,createdAt} = todo;
  return (
    <li className="todoList-new--list__item">
      <div className="todoList-new--list__item__action">
      <InfoCircleTwoTone />
      <CloseCircleOutlined style={{color : 'red'}}/>
      </div>
    <h5 className="todoList-new--list__item--title">{title}</h5>
    <Row className="todoList-new--list__item--tags" gutter={0}>
      <Col>
        <Tag color="success">success</Tag>
      </Col>
      <Col>
        <Tag color="error">success</Tag>
      </Col>
      <Col>
        <Tag color="warning">success</Tag>
      </Col>
    </Row>
    <Progress
      className="todoList-new--list__item--progress"
      format={(percent: any) => `${percent / 10}/10`}
      percent={10}
      steps={10}
    />
    <Row
      className="todoList-new--list__item--bottom"
    justify={'space-between'}>
      <Col className="todoList-new--list__item--bottom__date">
        <Tag bordered={false} color="processing">
          {moment(createdAt).format("DD-MM-YYYY")}
        </Tag>
      </Col>
      <Col className="todoList-new--list__item--bottom__action">
        <Row gutter={16}>
          <Col>
            <Row align={'middle'} gutter={4}>
              <Col>
              <CommentOutlined />
              </Col>
              <Col>
              <Typography.Text strong>7</Typography.Text>
              </Col>
            </Row>
          </Col>
          <Col>
          <Row align={'middle'} gutter={4}>
              <Col>
              <PaperClipOutlined />
              </Col>
              <Col>
              <Typography.Text strong>2</Typography.Text>
              </Col>
            </Row>
          </Col>
        </Row>
      </Col>
    </Row>
    <Avatar.Group
    size={'small'}
      maxCount={2}
      maxStyle={{
        color: '#f56a00',
        backgroundColor: '#fde3cf',
      }}
    >
      <Avatar size={'small'} src="https://xsgames.co/randomusers/avatar.php?g=pixel&key=2" />
      <Avatar
      size={'small'}
        style={{
          backgroundColor: '#f56a00',
        }}
      >
        K
      </Avatar>
      <Tooltip title="Ant User" placement="top">
        <Avatar
        size={'small'}
          style={{
            backgroundColor: '#87d068',
          }}
          icon={<UserOutlined />}
        />
      </Tooltip>
      <Avatar
      size={'small'}
        style={{
          backgroundColor: '#1677ff',
        }}
        icon={<AntDesignOutlined />}
      />
    </Avatar.Group>
  </li>
  )
}
