import requester from "./requester"

const todoList = {
    getAll : (query:any={}) => requester.get("/todoList",query)
}
export default todoList