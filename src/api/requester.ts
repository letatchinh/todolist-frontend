import axios from 'axios';
import { BASE_URL } from '../constants/defaultValue';

export function setupAxios() {
  axios.defaults.baseURL = `${BASE_URL}/api/v1/`;
  axios.defaults.headers.common['Content-Type'] = 'application/json';
}

export function setAxiosToken(token:any) {
  axios.defaults.headers.common.Authorization = 'Bearer ' + token;
}

export function removeAxiosToken() {
  axios.defaults.headers.common.Authorization = '';
}

const responseBody = (res:any) => res.data;

const requester = {
  get: (url:string, params:any={}, config = {}) =>
    axios
      .get(url, {
        params,
        ...config
      })
      .then(responseBody),

  post: (url:string, data:any, config = {}) =>
    axios.post(url, data, config).then(responseBody),
  postFormData: (url:string, data:any, config = {}) => {
    const bodyFormData = new FormData();
    bodyFormData.append('file', data);
    return axios({
      method: 'post',
      url,
      data: bodyFormData,
      headers: { 'Content-Type': 'multipart/form-data' }
    }).then(responseBody);
  },
  put: (url:string, data:any, config = {}) =>
    axios.put(url, data, config).then(responseBody),
  delete: (url:string, data:any) => axios.delete(url, { data }).then(responseBody)
};

export default requester;
