import requester from "./requester"

const user = {
    login : (query:any={}) => requester.post("/user/login",query),
    register : (query:any={}) => requester.post("/user/register",query),
    getProfile : (token:any) => requester.get(`/user/getProfile/${token}`),
}
export default user