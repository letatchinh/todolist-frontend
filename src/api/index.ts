import todoList from './todoList'
import user from './user'

export default {
    todoList,
    user,
}