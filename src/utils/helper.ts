export const fetcher = (url:any) => fetch(url).then(res => res.json());

export const getPaging = (response:any) => ({
    current: response.page,
    pageSize: response.limit,
    total: response.totalDocs
  });
    