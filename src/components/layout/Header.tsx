'use client'
import { useProfile } from '@/hook/user';
import { Button, Col, Row } from 'antd';
import Link from 'next/link';
import ProfileMenu from './user/ProfileMenu';

export default function Header() {
const profile = useProfile();
  
  return (
    // <header >
        <Row className='header--row' justify='space-between' align={'middle'}>
          <Col className='header--left'>
          {/* <Link href={'/'}>
          <Image alt='Logo'  src={Logo} width={50} height={50} priority  />
          </Link> */}
          </Col>
          <Col className='header--right'>
            {profile 
            ? <Row>
            <Col>
              <ProfileMenu />
            </Col>
          </Row>
            :  <Link href={'/login'}>
            <Button>Login</Button>
            </Link>
             }
            
      
          </Col>
        </Row>
    // </header>
  )
}
