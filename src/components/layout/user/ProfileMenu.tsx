"use client";
import { useProfile } from "@/hook/user";
import { DownOutlined } from "@ant-design/icons";
import { Avatar, Col, Dropdown, Row, Typography } from "antd";
import { get } from "lodash";
import React from "react";

export default function ProfileMenu() {
  const profile = useProfile();
  console.log(profile, "profile");

  return (
    <Dropdown
     trigger={["click"]}>
      <Row gutter={8} align={"middle"}>
        <Col>
          <Avatar
            style={{
              verticalAlign: "middle",
            }}
          >
            C
          </Avatar>
        </Col>
        <Col>
          <Typography.Text strong>
            {get(profile, "fullName", "")}
          </Typography.Text>
        </Col>
        <Col>
        <DownOutlined />
        </Col>
      </Row>
    </Dropdown>
  );
}
