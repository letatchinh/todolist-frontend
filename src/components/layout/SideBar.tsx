'use client'
import {
  AppstoreOutlined,
  LogoutOutlined,
  ShareAltOutlined,
} from "@ant-design/icons";
import Link from "next/link";
import React, { useEffect, useState } from "react";
import { usePathname } from 'next/navigation'
import { setupAxios } from "@/api/requester";

const items = [
  {
    path : '/dashboard',
    icon : <AppstoreOutlined />
  },
  {
    path : '/share',
    icon : <ShareAltOutlined />
  },
  {
    path : '/chill',
    icon : <i className="uil uil-music"></i>
  },
]
export default function SideBar() {
  const [active,setActive] = useState('');
  const pathname = usePathname();
  setupAxios();
  useEffect(() => {
    setActive(pathname)
  },[])
  return (
    <div className="sidebar">
      <ul className="sidebar--list">
        {items?.map(({path,icon}:any) => <Link onClick={() => setActive(path)} href={path}>
          <li className={`sidebar--list__item ${active === path ? 'active' : ''}`}>
            {icon}
          </li>
        </Link>)}
        <li className="sidebar--list__item">
          <LogoutOutlined />
        </li>
      </ul>
    </div>
  );
}
