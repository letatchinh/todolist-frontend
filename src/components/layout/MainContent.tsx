'use client'
import { setAxiosToken, setupAxios } from "@/api/requester";
import { useGoBackWhenLogin, useToken } from "@/hook/user";
import { useRouter } from "next/navigation";
import React, { useEffect } from "react";
import SideBar from "./SideBar";

export default function MainContent({children}:{
    children: React.ReactNode;
}) {
  const token = useToken();
  const router = useRouter();
  const redirect = useGoBackWhenLogin();
  setupAxios();
  setAxiosToken(token);
    useEffect(() => {
      if(!token){
        router.replace("/login");
      }else{
        router.replace(redirect ?? "/");
      }
    },[token]);
    if(!token){
        return <div className="main-content">
        <div className="content">{children}</div>
      </div>
    }
  return (
    <div className="main-content">
      <SideBar />
      <div className="content">{children}</div>
    </div>
  );
}
