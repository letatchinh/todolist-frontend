import moment from "moment";
import { useSearchParams } from "next/navigation";
import { useEffect } from "react";
import { toast } from "react-hot-toast";
import { useDispatch, useSelector } from "react-redux";
import {isInteger} from 'lodash';

export const useSuccess = (successSelector:any, mess?:string, onSuccess?:any) => {
    const success = useSelector(successSelector);
    useEffect(() => {
      if (success) {
        if (mess) {
            toast.success(mess);
        }
  
        if (onSuccess) onSuccess(success);
      }
      //eslint-disable-next-line
    }, [success, mess]);
  };
  
  export const useFailed = (failedSelector:any, mess?:string, onFailed?:any, mute = false) => {
    const failed:any = useSelector(failedSelector);
    useEffect(() => {
      if (failed && !mute) {
        toast.error(
          mess || failed?.response?.data?.message || 'Something went wrong!'
        );
      }
  
      if (onFailed) onFailed(failed);
    }, [failed, mess, onFailed]);
  };

  export const useFetchByParam = (props:any) => {
    const {
      action,
      dataSelector,
      failedSelector,
      loadingSelector,
      param,
      muteOnFailed,
      actionUpdate
    } = props;
    const dispatch = useDispatch();
    const data = useSelector(dataSelector);
    const isLoading = useSelector(loadingSelector);
    useEffect(() => {
      if (param) dispatch(action(param));
      // else dispatch(action(param));
    }, [dispatch, action, param]);
  
    useFailed(failedSelector, undefined, undefined, muteOnFailed);
  
    const useUpdateData = (dataUpdate:any) => { // Update Data To Redux
      if (actionUpdate && typeof actionUpdate === 'function') {
        dispatch(actionUpdate(dataUpdate));
      }
    }
    return [data, isLoading, useUpdateData];
  };


  export const useFetch = (props:any) => {
    const { action, dataSelector, failedSelector, loadingSelector,payload } = props;
  
    const dispatch = useDispatch();
    const data = useSelector(dataSelector);
    const isLoading = useSelector(loadingSelector);
    useEffect(() => {
      dispatch(action());
    }, [dispatch, action, payload]);
  
    useFailed(failedSelector);
  
    return [data, isLoading];
  };
  interface PayloadUseSubmit {
    loadingSelector  : any,
    action : any,
  }
  export const useSubmit = ({ loadingSelector, action }:PayloadUseSubmit) => {
    const dispatch = useDispatch();
    const isLoading = useSelector(loadingSelector);
  
    const handleSubmit = (values:any) => {
      dispatch(action(values));
    };
  
    return [isLoading, handleSubmit];
  };
  
  export const useQueryParams = () => {
    return new URLSearchParams(useSearchParams());
  };
  
  export const useResetState = (resetAction:any) => {
    const dispatch = useDispatch();
    useEffect(() => {
      return () => {
        dispatch(resetAction());
      };
    }, [dispatch, resetAction]);
  };

  export const getSelectors = (moduleName:string) => {
    const getSelector = (key:any) => (state:any) => state[moduleName][key];
  
    return {
      loadingSelector: getSelector('isLoading'),
      listSelector: getSelector('list'),
      getListFailedSelector: getSelector('getListFailed'),
  
      getByIdLoadingSelector: getSelector('isGetByIdLoading'),
      getByIdSelector: getSelector('byId'),
      getByIdFailedSelector: getSelector('getByIdFailed'),
  
      deleteSuccessSelector: getSelector('deleteSuccess'),
      deleteFailedSelector: getSelector('deleteFailed'),
  
      isSubmitLoadingSelector: getSelector('isSubmitLoading'),
      createSuccessSelector: getSelector('createSuccess'),
      createFailedSelector: getSelector('createFailed'),
  
      updateSuccessSelector: getSelector('updateSuccess'),
      updateFailedSelector: getSelector('updateFailed'),
      pagingSelector: getSelector('paging')
    };
  };

  export const vietnamMoment =(v:any,formatTime:any)=> {
    if(v){
      const utcMoment = moment.utc(v);
      if(formatTime){
        return utcMoment.format(formatTime);
      }
      else{
        return utcMoment
      }
    }
    return null
   
  }

  export const formatter = (num:any, fixed = 2) => {
    if(!num) return 0
    const parsedNum = parseFloat(num);
    if (isInteger(parsedNum) && parsedNum === 0) {
      return num?.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    } else if (Number.isInteger(parsedNum)) {
      return parsedNum.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    } else {
      return parsedNum.toFixed(fixed).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
  };