import { getProfileAction, loginAction } from "@/redux/user/action"
import { usePathname } from "next/navigation";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useFailed, useFetch, useFetchByParam, useSubmit, useSuccess } from "../untils"

const getSelector = (key:string) => (state:any) => state.user[key];
const tokenSelector = getSelector('token');
const isLoadingSelector = getSelector('isLoading');
const profileSelector = getSelector('profile');
const loginFailedSelector = getSelector('loginFailed');
const getProfileFailedSelector = getSelector('getProfileFailed');
const isGetProfileLoadingFailedSelector = getSelector('isGetProfileLoading');
export const useLogin = () => {
    useSuccess(tokenSelector,"Đăng nhập thành công");
    useFailed(loginFailedSelector,"Đăng nhập thất bại")
    return useSubmit({
        loadingSelector : isLoadingSelector,
        action : loginAction
    })
};

export const useProfile = () => {
    const profile = useSelector(profileSelector);
    return profile
};
export const useToken = () => {
    const token = useSelector(tokenSelector);
    return token
};

export const useGoBackWhenLogin = () => {
    const [redirect,setRedirect]:any = useState();
    const pathname = usePathname();
    useEffect(() => {
        setRedirect(pathname);
    },[]) ;
    return redirect
}
export const useGetProfile = () => {
    const token = useSelector(tokenSelector);
    return useFetchByParam({
        action : getProfileAction,
        dataSelector : profileSelector,
        failedSelector : getProfileFailedSelector,
        loadingSelector : isGetProfileLoadingFailedSelector,
        param : token
    })
}